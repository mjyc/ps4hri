import "./styles.css";

import xs from "xstream";
import { run } from "@cycle/run";
import { div, textarea, hr, label, pre, makeDOMDriver } from "@cycle/dom";
import speclang from "speclang";

const makeEditor = specName => {
  const className = `.${specName}textarea`;
  const parse = code => {
    let ast, errMsg;
    try {
      ast = speclang[specName].parser.parse(code);
    } catch (e) {
      errMsg = speclang.errMsgStr(e, code);
    }
    return {
      code,
      ast,
      errMsg
    };
  };
  const format = ast => {
    const maxLen = Math.max(...ast.map(x => x.desc.length));
    return ast
      .map(
        x =>
          `${x.label}: ${x.values
            .map(val => (val === true ? "t" : val === false ? "f" : val))
            .join("")}`
      )
      .join("\n");
  };
  return sources =>
    xs
      .merge(
        sources.DOM.select(className) // parse
          .events("input")
          .map(ev => ev.target.value)
          .startWith("")
          .map(parse),
        sources.DOM.select(className) // format
          .events("blur")
          .map(ev => ev.target.value)
          .map(parse)
          .filter(
            ({ ast }) =>
              typeof ast !== "undefined" && specName === "marblediagram"
          )
          .map(({ code, ast, errMsg }) => ({ code: format(ast), ast, errMsg }))
      )
      .map(({ code, ast, errMsg }) =>
        div([
          label(`${specName} DSL`),
          textarea(className, {
            attrs: { type: "text" },
            props: { value: code }
          }),
          pre(!!errMsg ? errMsg : JSON.stringify(ast, null, 2)),
          hr()
        ])
      );
};

function main(sources) {
  const marble$ = makeEditor("marblediagram")(sources);
  const evsc$ = makeEditor("eventscript")(sources);
  const ifttt$ = makeEditor("ifttt")(sources);
  const vdom$ = xs.combine(marble$, evsc$, ifttt$).map(vdoms => div(vdoms));

  return {
    DOM: vdom$
  };
}

run(main, {
  DOM: makeDOMDriver("#app")
});
