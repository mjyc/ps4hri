const marblediagram = require("./marblediagram");
const eventscript = require("./eventscript");
const ifttt = require("./ifttt");

const errMsgStr = (err, codeStr) => `Parsing error on line ${
  err.location.start.line
}:
...${codeStr.split("\n")[err.location.start.line - 1]}
${"-".repeat(3 + err.location.start.column - 1) + "^"}
${err.message}`;

module.exports = { marblediagram, eventscript, ifttt, errMsgStr };
