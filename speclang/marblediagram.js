const pegjs = require("pegjs");

const grammar = `
spec
  = head:stream tail:(br stream)* _ {
    const tmp = tail.reduce((acc, x) => acc.concat([x[1]]), [head]);
    const descMaxLen = Math.max(...tmp.map(x => x.desc.length));
    const valuesMaxLen = Math.max(...tmp.map(x => x.values.length));
    return tmp.map(x => ({
      type: x.type,
      desc: x.desc,
      label: x.desc + " ".repeat(descMaxLen - x.desc.length),
      values: x.values.concat(Array(valuesMaxLen - x.values.length).fill("-"))
    }));
  }

stream
  = desc:(sensorDescriptor / actionDescriptor) ws ":" ws values:events {
    return {
      type: desc.type,
      desc: desc.value,
      values: values
    };
  }

events
  = head:event tail:event* {
    return tail.reduce((acc, x) => acc.concat(x), [head]);
  }

sensorDescriptor
  = desc:("ready" / "isFaceVisible" / "faceLookingAt" / "headTiltedTo" / "isHumanSpeaking" / "sayFinished" / "playSoundFinished") {
    return {
      type: "sensor",
      value: text()
    };
  }

actionDescriptor = desc:("state" / "setMessage" / "say") {
  return {
    type: "action",
    value: text()
  };
}

event = evt:(noevent / boolean / number / character) {
  return evt;
}

noevent = "-" {
  return text();
}

boolean = ("f" / "t") {
  return text() === "t";
}

number = [0-9] {
  return parseInt(text(), 10);
}

character = [a-zA-Z]

_ "blank" = [ \\t\\r\\n]*

ws "whitespace" = [ \\t]*

br "linebreak" = [\\r\\n]+
`;

const parser = pegjs.generate(grammar);

module.exports = { parser };
