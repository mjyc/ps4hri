const pegjs = require("pegjs");

const grammar = `
spec
  = head:rule tail:(br rule)* _ {
    return tail.reduce((acc, x) => acc.concat(x[1]), [head]);
  }

rule
  = "if" ws cond:condition ws "then" ws a:action {
    return {
      condition: cond,
      action: a
    };
  }

condition = desc:sensorDescriptor ws "is" ws val:(boolean / number / string) {
  return {
    type: desc.type,
    desc: desc.value,
    value: val
  };
}

action = desc:actionDescriptor ws "is" ws val:(boolean / number / string) {
  return {
    type: desc.type,
    desc: desc.value,
    value: val
  };
}

sensorDescriptor
  = desc:("ready" / "isFaceVisible" / "faceLookingAt" / "headTiltedTo" / "isHumanSpeaking" / "sayFinished" / "playSoundFinished") {
    return {
      type: "sensor",
      value: text()
    };
  }

actionDescriptor = desc:("state" / "setMessage" / "say") {
  return {
    type: "action",
    value: text()
  };
}

boolean = ("true" / "false") {
  return text() == "true";
}

number = [0-9]+ {
  return parseInt(text(), 10);
}

string = '"' [a-zA-Z0-9_$:.,?! ]* '"' {
  return text();
}

_ "blank" = [ \\t\\r\\n]*

ws "whitespace" = [ \\t]*

br "linebreak" = [\\r\\n]+
`;

const parser = pegjs.generate(grammar);

module.exports = { parser };
