const pegjs = require("pegjs");

const grammar = `
spec
  = _ head:event ws tail:(br ws event ws)* _ {
    return tail.reduce((acc, x) => acc.concat(x[2]), [head]);
  }

event
  = offset:duration? ws name:("ready" / "isFaceVisible" / "faceLookingAt" / "isHumanSpeaking" / "sayFinished" / "say" / "setMessage") ws ":" ws value:(string / number / boolean) {
    return {
      name: name,
      value: value,
      offset: offset === null ? -1 : offset
    }
  }

duration = "+" value:number {
  return value;
}

boolean = ("true" / "false") {
  return text() == "true";
}

number = [0-9]+ {
  return parseInt(text(), 10);
}

string = '"' [a-zA-Z0-9_$:.,?! ]* '"' {
  return text();
}

_ "blank" = [ \\t\\r\\n]*

ws "whitespace" = [ \\t]*

br "linebreak" = [\\r\\n]+
`;

const parser = pegjs.generate(grammar);

module.exports = { parser };
