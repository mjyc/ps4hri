#lang rosette/safe

(provide (all-defined-out))

(require rackunit rackunit/text-ui
 (only-in racket/base build-list)
 rosette/lib/angelic
 (prefix-in s/ frpsynth/streams/disctime)
 (prefix-in l/ frpsynth/lang)
 (prefix-in logger- "../logger.rkt")
 )


(define (test-verify-action-sequence)
  (test-case
    "test-verify-action-sequence"
    (logger-debug "================")
    (logger-debug "test-verify-action-sequence")
    (logger-debug "================\n")

    (define numinputs 2)
    (define insts
      (list
        (l/mapTo 1 (l/register 0))
        (l/map add1 (l/register 1))
        (l/merge (l/register 2) (l/register 3))
        )
      )
    (define prog
      (l/program
        numinputs
        insts
        ))
    (logger-debug "prog:")
    (logger-debug (l/program->string prog))
    (define insts2
      (list
        (l/delay 2 (l/register 0)) ; 2
        (l/delay 4 (l/register 0)) ; 3
        (l/delay 6 (l/register 0)) ; 4
        (l/mapTo 1 (l/register 0)) ; 5
        (l/mapTo 2 (l/register 2)) ; 6
        (l/mapTo 3 (l/register 3)) ; 7
        (l/mapTo 4 (l/register 4)) ; 8
        (l/merge (l/register 5) (l/register 6))
        (l/merge (l/register 7) (l/register 8))
        (l/merge (l/register 9) (l/register 10))
        )
      )
    (define spec
      (l/program
        numinputs
        insts2
        ))
    (logger-debug "spec:")
    (logger-debug (l/program->string spec))

    (define inputs
      (list
        (s/from-diagram "t-------")
        (s/from-diagram "--1-2-3-")
        )
      )
    (define output (s/program-interpret prog inputs))
    (logger-debug "prog output:")
    (logger-debug output)
    (logger-debug "")
    (define output2 (s/program-interpret spec inputs))
    (logger-debug "spec output:")
    (logger-debug output2)
    (logger-debug "")

    (define input-length 8)
    (define sym-inputs
      (list
        (s/from-diagram "t-------")
        (s/??stream (lambda () (choose* 1 2 3)) input-length)
        )
      )
    (define sym-output (s/program-interpret prog sym-inputs))
    (define sym-output2 (s/program-interpret spec sym-inputs))

    (define cex (verify
      ; commenting the below out will
      ; #:assume (assert
      ;   (equal?
      ;     sym-output
      ;     (s/delay 2 (second sym-inputs))
      ;     )
      ;   )
      #:guarantee (assert
        (equal?
          sym-output
          sym-output2)
        )
      ))
    (check-true (sat? cex))

    (logger-debug "counter example:")
    (logger-debug  (evaluate (second sym-inputs) cex))
    )
  )


; Main

(module+ test
  (define/provide-test-suite hriprog_ver_tests
    (test-verify-action-sequence)
    )
  (run-tests hriprog_ver_tests)
  )
