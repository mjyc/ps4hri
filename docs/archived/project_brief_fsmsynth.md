# Title

End-user Programming for Social Robots as FSM Program Synthesis

<!-- Social robots behavior authoring as program synthesis -->
<!-- Authoring social robots behavior using program synthesis -->


# Abstract

## Contributions

* **end-user programming for social robots as programming synthesis**
  * representing **HRI patterns** as specs
  * representing **user inputs** as specs
    * NOTE: user inputs could be (temporal) instructions, constraints, and preferences
  * **synthesis algorithm** for a robot FSM (e.g., a mealy machine)
    * Representing FSM 
    * An algorithm for **creating sketch** from user inputs and HRI patterns
    * An algorithm to **convert** user inputs and HRI patterns **into specs**
    * Handling of **temporal specs**
* algorithm analysis & results
  * TODO: see results of holtz, newcomb, and other synthesis papers
* (optional) framing social robot behavior authoring (programming) as reactive state-machine programming
  * to address concurrency problems when programming social robot behaviors

### Potential contributions ideas

* FSM in FRP
* Use of LTL, a temporal spec
* Modeling environment (e.g., a human simulator)



# (TODO: Need update) Introduction

## Need

For social robots to be truly successful, they need to provide a large amount of useful contents, e.g., educational or entertaining contents.
Ideally, we want to let end users to create contents for social robots, ...
<!-- however, creating contents or programming social robots is even difficult for programmers because social robot behaviors(programs) (i) are highly concurrent and (ii) need to follow social norms. -->

TODO: For the "need" paragraph, motivate this project with what I've seen in the field, Ross's and Emilia's experiences, and Alexa + Google home guidelines.

## Approach

<!-- To address these problems, we first frame programming social robots as reactive state-machine programming to handle the concurrency issue and present a program repair method for helping programmers to author robot behaviors that follow social norms and exhibits a certain "personality". -->

TODO: Update the "approach" section with a condense version of the program synthesis work I've done. Remember to highlight the HRI specs, user input as temporal specs aspect.

## Benefits

TODO:
* end-user does not need to program
* synthesized behaviors have guarantees
* 

## Competitions

TODO:
* alternatively
  * you could use end-user programming tools (mention the failure of Jibo and many other platforms)
    * even the ones you need
  * use template
  * learn the behavior End2End
  * use planner? or POMDP?


# Related Work

## Theme1: Robot programming systems

* EUP for social robots
* Programming by instructions (cite cones from the classical planning ppl)

## Theme2: Program repair/synthesis/verification application in robotics

* old papers on space robot behavior verification, e.g., hybrid system verification, golog verification
  * TOODs: look into ORCCAD, Robotic Systems Architectures and Programming, Experiences in applying formal verification in robotics
* HRI robot verification and synthesis for safety
* verifying social norms

## Theme3: Misc.

* Learning, template approach, etc.
* Instruction executioners & planners
* Solver-aided language application in HCI?



# Method

See [algorithm_sketch.md](./algorithm_sketch.md).



# Evaluation

See [algorithm_evaluation.md](./algorithm_evaluation.md).



# Limitations & Future directions & Conclusion

TODO: Are there any other topics to discuss here?

## Limitations

* ...

## Future directions

* visual interface
* scaling up
* handling uncertainty
* authoring specs



# Milestones

| # | Key Results | Due Date |
|---|-------------|----------|
| ~~M1~~ | ~~Program social robot behaviors with personality v1~~ | ~~10/12~~ 11/2 |
| ~~M2~~ | ~~Develop a program repair method v1~~ | ~~11/02~~ 11/16 |
| M3 | Develop a program synthesis algorithm | 12/9 |
| M4 | Evaluate the program synthesis algorithm and write up everything| 12/30 |
| M5 | Improve the synthesis algorithm and do more evaluation | 1/20 |
| M6 | Write new results | 2/1 |

## M3

* Come up with a data collection pipeline; iterate quickly
* Refine the method as the data is rolling in


## M4
