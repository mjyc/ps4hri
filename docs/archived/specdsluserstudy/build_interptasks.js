#!/usr/bin/env node

const fs = require("fs");

// const typeToName = {
//   marble: "Marble Diagram",
//   evsc: "Event Script",
//   ifttt: "IFTTT",
//   ltl: "LTL"
// };

const typeToFormLink = {
  marble:
    "https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform",
  evsc:
    "https://docs.google.com/forms/d/e/1FAIpQLScx5cSffTHtEvVwBSYVK8hL_ZyLX0ti7S79QQG_aBD1GBZECg/viewform",
  ifttt:
    "https://docs.google.com/forms/d/e/1FAIpQLSdK0TOIr15w3NwO08Bo5sc-npOQen8wvCRUxW9GCZ-XaVby4A/viewform",
  ltl:
    "https://docs.google.com/forms/d/e/1FAIpQLSdsUNDeEWEmT_96Qo_xGnD2X96ge4XcaRAep23FwEHp8QC1Rw/viewform"
};

const build = (mainStr, interptasksStr, link) => {
  const outStr = String(mainStr).replace(
    "{interptasks}",
    interptasksStr.replace(/Google Form/g, `[Google Form](${link})`)
  );
  return outStr;
};

if (typeof process.argv[2] === "undefined") {
  Object.keys(typeToFormLink).map(type => {
    const mainStr = String(fs.readFileSync(`./interpmain.md`));
    const interptasksStr = String(fs.readFileSync(`./interptasks_${type}.md`));
    const outStr = build(mainStr, interptasksStr, typeToFormLink[type]);
    fs.writeFileSync(`./interp_${type}.md`, outStr);
  });
} else {
  const type = process.argv[2];
  const mainStr = String(fs.readFileSync(`./interpmain.md`));
  const interptasksStr = String(fs.readFileSync(`./interptasks_${type}.md`));
  const outStr = build(mainStr, interptasksStr, typeToFormLink[type]);
  console.log(outStr);
}
