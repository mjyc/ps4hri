<!-- '-' means 1 sec -->
<!-- 'sensor' emits state chances except sayFinished -->
<!-- 'robot' emits events -->

### Interpretation Task 1

<!-- timeout -->

```
sensor.ready    : f-t-----------
robot.setMessage: --a--b--c--d--

a="Hello"
b="My name is TaRo"
c="Nice to meet you"
d="Bye"
```

Please write your answer in the Google Form.

### Interpretation Task 2

<!-- loop -->

```
sensor.ready      : f-t------------
sensor.sayFinished: -----a---b--a--
robot.say         : --a--b---a--b--

a="Hello there"
b="Have a great day"
```

Please write your answer in the Google Form.

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' starting with !"right" and emits no more state change events -->

```
sensor.ready         : f-t-------
sensor.humanLookingAt: n----r----
robot.say            : --a-----b-

n=!"right"
r="right"
a="Rotate your head to the right"
b="Great job"
```

```
sensor.ready         : f-t-------
sensor.humanLookingAt: r---------
robot.say            : --a--b----

r="right"
a="Rotate your head to the right"
b="Great job"
```

```
sensor.ready         : f-t-------
sensor.humanLookingAt: n------l--
robot.say            : --a----a--

n=!"right" && !"left"
l="left"
a="Rotate your head to the right"
```

Please write your answer in the Google Form.

### Interpretation Task 4

<!-- counting -->

<!-- does not cover (i) the case 'humanLookingAt' starting with "right" and (ii) the case 'humanLookingAt' emits "right" after ready=true -->

```
sensor.ready         : f-t----------------
sensor.humanLookingAt: i-----j--k-----l---
robot.say            : --a---a--a-----b---

i=!"right"
j=!"right"
k=!"right"
l=!"right"
a="Rotate your head to the right"
b="Bye"
```

Please write your answer in the Google Form.

### Interpretation Task 5

<!-- order -->

```
sensor.isHumanSpeaking: f--t---f-----
robot.say             : -------a-----

a="Hi"
```

Please write your answer in the Google Form.

### Interpretation Task 6

<!-- combine -->

```
sensor.humanLookingAt : n--c---------
sensor.isHumanSpeaking: f------------
robot.say             : ---a---------

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n-----c-----
sensor.isHumanSpeaking: f----t---f--
robot.say             : ---------a--

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n--------c--
sensor.isHumanSpeaking: f--t---f----
robot.say             : ---------a--

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n------c----
sensor.isHumanSpeaking: f--t---f----
robot.say             : -------a----

n=!"center"
c="center"
a="Hi"
```

Please write your answer in the Google Form.
