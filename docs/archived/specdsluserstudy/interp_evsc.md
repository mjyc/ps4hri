# Robot Behavior Creation Tool User Study

The purpose of this study is to investigate the usability of a social robot behavior creation tool.

## Meet TaRo

<img src="./figs/taro1.jpg" height="150"/>
<img src="./figs/taro2.jpg" height="150"/>
<img src="./figs/taro3.jpg" height="150"/>

TaRo (stands for "Tablet Robot") is a robot face on a screen that can

- detect a human face
- detect the face direction of the human face
- sense voice activity

and

- display messages on screen
- speak via text-to-speech

## Robot Language

Image you can instruct TaRo do something by giving it instructions that only robots can understand--like a robot language!

## Robot Language Interpretation Tasks

Please examine instructions written in the robot language and describe what you think TaRo will do in plain English. _There is no right or wrong answer._

### Interpretation Task 1

<!-- timeout -->

```
sensor.ready=true
  robot.setMessage="Hello"
  AFTER 3s robot.setMessage="My name is TaRo"
  AFTER 6s robot.setMessage="Nice to meet you"
  AFTER 9s robot.setMessage="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLScx5cSffTHtEvVwBSYVK8hL_ZyLX0ti7S79QQG_aBD1GBZECg/viewform).

### Interpretation Task 2

<!-- loop -->

```
sensor.ready=true
  robot.say="Hello there"
```

```
sensor.sayFinished="Hello there"
  robot.say="Have a great day"
```

```
sensor.sayFinished="Have a great day"
  robot.say="Hello there"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLScx5cSffTHtEvVwBSYVK8hL_ZyLX0ti7S79QQG_aBD1GBZECg/viewform).

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' does nothing after 'robot.say="Rotate your head to the right"' -->

```
sensor.ready=true
  robot.say="Rotate your head to the right"
sensor.humanLookingAt="right"
  AFTER 3s robot.say="Great job"
```

```
sensor.humanLookingAt="right"
sensor.ready=true
  robot.say="Rotate your head to the right"
  AFTER 3s robot.say="Great job"
```

```
sensor.ready=true
  robot.say="Rotate your head to the right"
sensor.humanLookingAt="left"
  robot.say="Rotate your head to the right"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLScx5cSffTHtEvVwBSYVK8hL_ZyLX0ti7S79QQG_aBD1GBZECg/viewform).

### Interpretation Task 4

<!-- counting -->

```
sensor.ready=true
  robot.say="Rotate your head to the right"
sensor.humanLookingAt=!"right"
  robot.say="Rotate your head to the right"
sensor.humanLookingAt=!"right"
  robot.say="Rotate your head to the right"
sensor.humanLookingAt=!"right"
  robot.say="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLScx5cSffTHtEvVwBSYVK8hL_ZyLX0ti7S79QQG_aBD1GBZECg/viewform).

### Interpretation Task 5

<!-- order -->

```
sensor.isHumanSpeaking=true
sensor.isHumanSpeaking=false
  robot.say="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLScx5cSffTHtEvVwBSYVK8hL_ZyLX0ti7S79QQG_aBD1GBZECg/viewform).

### Interpretation Task 6

<!-- combine -->

<!-- does not cover the case 'humanLookingAt' and 'isHumanSpeaking' emit false and "center" at the same time -->

```
sensor.humanLookingAt="center"
sensor.isHumanSpeaking=false
  robot.say="Hi"
```

```
sensor.isHumanSpeaking=false
sensor.humanLookingAt="center"
  robot.say="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLScx5cSffTHtEvVwBSYVK8hL_ZyLX0ti7S79QQG_aBD1GBZECg/viewform).


## Post Study Questionnaire

Please answer the remaining questions in the Google Form.
