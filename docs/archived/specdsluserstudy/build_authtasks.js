#!/usr/bin/env node

const fs = require("fs");

// const typeToName = {
//   marble: "Marble Diagram",
//   evsc: "Event Script",
//   ifttt: "IFTTT",
//   ltl: "LTL"
// };

const typeToFormLink = {
  marble: "https://forms.gle/ezWiWM8Rx4PP3uzm6",
  // evsc: "",
  // ifttt: "",
  // ltl: ""
};

const build = (mainStr, tasksStr, link) => {
  const outStr = String(mainStr).replace("{authtasks}", tasksStr);
  return outStr.replace(/Google Form/g, `[Google Form](${link})`);
};

if (typeof process.argv[2] === "undefined") {
  Object.keys(typeToFormLink).map(type => {
    const mainStr = String(fs.readFileSync(`./authmain_${type}.md`));
    const tasksStr = String(fs.readFileSync(`./authtasks.md`));
    const outStr = build(mainStr, tasksStr, typeToFormLink[type]);
    fs.writeFileSync(`./auth_${type}.md`, outStr);
  });
} else {
  const type = process.argv[2];
  const mainStr = String(fs.readFileSync(`./authmain_${type}.md`));
  const tasksStr = String(fs.readFileSync(`./authtasks.md`));
  const outStr = build(mainStr, tasksStr, typeToFormLink[type]);
  console.log(outStr);
}
