### Interpretation Task 1

<!-- timeout -->

```
IF ready=true THEN setMessage="Hello"
    && AFTER 3s setMessage="My name is TaRo"
    && AFTER 6s setMessage="Nice to meet you"
    && AFTER 9s setMessage="Bye"
```

Please write your answer in the Google Form.

### Interpretation Task 2

<!-- loop -->

```
IF ready=true || sayFinished="Have a great day"
  THEN say="Hello there"
      && EVENTUALLY IF sayFinished="Hello there" THEN say="Have a great day"
```

Please write your answer in the Google Form.

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' does nothing after 'robot.say="Rotate your head to the right"' -->

```
IF humanLookingAt=!"right"
  THEN EVENTUALLY IF ready=true THEN say="Rotate your head to the right"
      && EVENTUALLY IF humanLookingAt="right"
        THEN AFTER 3s say="Great job"
IF humanLookingAt="right"
  THEN EVENTUALLY IF ready=true THEN say="Rotate your head to the right" && AFTER 3s say="Great job"
IF ready=true THEN say="Rotate your head to the right"
    && EVENTUALLY IF humanLookingAt=!"right" THEN AFTER 3s say="Rotate your head to the right"
```

Please write your answer in the Google Form.

### Interpretation Task 4

<!-- counting -->

```
IF humanLookingAt=!"right"
  THEN EVENTUALLY ready=true
      && say="Rotate your head to the right"
      && EVENTUALLY IF humanLookingAt=!"right"
        THEN EVENTUALLY IF humanLookingAt=!"right"
          THEN EVENTUALLY IF humanLookingAt=!"right"
            THEN say="Bye"
```

Please write your answer in the Google Form.

### Interpretation Task 5

<!-- order -->

```
IF isHumanSpeaking=false THEN EVENTUALLY IF isHumanSpeaking=true THEN say="Hi"
```

Please write your answer in the Google Form.

### Interpretation Task 6

<!-- combine -->

```
IF humanLookingAt="center" && isHumanSpeaking=false THEN say="Hi"
IF humanLookingAt="center" THEN EVENTUALLY isHumanSpeaking=false && say="Hi"
IF isHumanSpeaking=false THEN EVENTUALLY humanLookingAt="center" && say="Hi"
```

Please write your answer in the Google Form.
