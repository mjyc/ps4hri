# Robot Behavior Creation Tool User Study

The purpose of this study is to investigate the usability of a social robot behavior creation tool.

## Meet TaRo

<img src="./figs/taro1.jpg" height="150"/>
<img src="./figs/taro2.jpg" height="150"/>
<img src="./figs/taro3.jpg" height="150"/>

TaRo (stands for "Tablet Robot") is a robot face on a screen that can

- detect a human face
- detect the face direction of the human face
- sense voice activity

and

- display messages on screen
- speak via text-to-speech

## Robot Language

Image you can instruct TaRo do something by giving it instructions that only robots can understand--like a robot language!

## Robot Language Interpretation Tasks

Please examine instructions written in the robot language and describe what you think TaRo will do in plain English. _There is no right or wrong answer._

{interptasks}

## Post Study Questionnaire

Please answer the remaining questions in the Google Form.
