# Robot Behavior Creation Tool User Study

The purpose of this study is to investigate the usability of a social robot behavior creation tool.

## Meet TaRo

<img src="./figs/taro1.jpg" height="150"/>
<img src="./figs/taro2.jpg" height="150"/>
<img src="./figs/taro3.jpg" height="150"/>

TaRo (stands for "Tablet Robot") is a robot face on a screen that can

- detect a human face
- detect the face direction of the human face
- sense voice activity

and

<!-- - display messages on screen -->
- speak via text-to-speech

## Robot Language

Image you can instruct TaRo do something by giving it instructions that only robots can understand--like a robot language!

**TODO** decide on whether to provide examples or run interp tasks before authoring tasks

The robot language we use in this study is consisted of the following parts:

**TODO** finish the rest of the "Robot Language" subsection
- `-` the passage of time without any events
- `1` numbers 0-9 are treated as literal numeric values
- `a` other literal values are strings

For example,

```
sensors.isFaceVisible: f--t---f-t----
actions.setMessage   : 0--1---0-1----
actions.say          : g--h---g-h----

h="hello"
g="goodbye"
```

means ...

## Robot Behavior Creation Tasks

{authtasks}

## Post Study Questionnaire

Please answer the remaining questions in the Google Form.

**TODO**
- add SUS questions
- add subjective expressivity question(s)
