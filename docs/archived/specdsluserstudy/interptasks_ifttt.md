<!-- precedence: ! > && > || -->

### Interpretation Task 1

<!-- timeout -->

```
IF ready=true THEN setMessage="Hello"
IF 3s SINCE ready=true THEN setMessage="My name is TaRo"
IF 6s SINCE ready=true THEN setMessage="Nice to meet you"
IF 9s SINCE ready=true THEN setMessage="Bye"
```

Please write your answer in the Google Form.

### Interpretation Task 2

<!-- loop -->

```
IF ready=true || sayFinished="Have a great day" THEN say="Hello there"
IF sayFinished="Hello there" THEN say="Have a great day"
```

Please write your answer in the Google Form.

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' does nothing after 'robot.say="Rotate your head to the right"' -->

<!-- could be re-written with 4 IF ... THEN ... s -->
```
IF LAST ready=true
    || LAST ready=true && humanLookingAt=!"right"
  THEN say="Rotate your head to the right"
IF LAST ready=true && 3s SINCE humanLookingAt="right"
    || LAST humanLookingAt="right" && 3s SINCE ready=true
  THEN say="Great job"
```

Please write your answer in the Google Form.

### Interpretation Task 4

<!-- counting -->

```
IF LAST ready=true THEN say="Rotate your head to the right"
IF LAST ready=true && 3rd humanLookingAt=!"right" THEN say="Bye"
```

Please write your answer in the Google Form.

### Interpretation Task 5

<!-- order -->

```
IF LAST isHumanSpeaking=false && isHumanSpeaking=true THEN say="Hi"
```

Please write your answer in the Google Form.

### Interpretation Task 6

<!-- combine -->

```
IF humanLookingAt="center" && isHumanSpeaking=false
    || LAST humanLookingAt="center" && isHumanSpeaking=false
    || LAST isHumanSpeaking=false && humanLookingAt="center"
  THEN say="Hi"
```

Please write your answer in the Google Form.
