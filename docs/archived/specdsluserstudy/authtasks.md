<!-- timeout -->
<!-- loop -->
<!-- branching -->
<!-- counting -->
<!-- order -->
<!-- combine -->

## Storytelling
<!--  / Monologue -->

<!-- timeout -->
<!-- combine -->

Please implement and test a program that meets the below descriptions using [this online text editor](). Once you are done please submit your program using the Google Form, i.e., copy your program in the editor and paste it as an answer to the corresponding question in the Google Form.

- When the robot is ready and a human's face appears in the robot's field of view, it should say "My name is TaRo".<!--  and make "happy" face expression. -->

- If it has been at least 1 seconds since the robot finished saying "My name is Meebo" and the human's face is visible and the human is not speaking currently, then the robot should say "I am made of a touch monitor and a robot arm".

- If it has been at least 1 seconds since the robot finished saying "I am made of a touch monitor and a robot arm" and the human's face is visible and the human is not speaking currently, then the robot should say display "Nice to meet you", say "Nice to meet you", and make "happy" face expression.

- If the robot has not seen the human for 5 seconds, it should say "Bye".<!--  and make "sad" expression. -->

## Neck Exercise
<!--  / Instructions -->

<!-- branching -->
<!-- loop -->

Please implement and test a program that meets the below descriptions using [this online text editor](). Once you are done please submit your program using the Google Form, i.e., copy your program in the editor and paste it as an answer to the corresponding question in the Google Form.

- When the robot is ready and the human's head is not tilted, it should say "Tilt your head to the right".

- When the robot finished saying "Tilt your head to the right" and the human's head is tilted to the right, the robot should say "and now slowly rotate to your left".

- When the human's head is not tilted to right after the robot started saying "Tilt your head to the right", the robot should say "Tilt your head to the right" again.

- When the robot finished saying "Tilt your head to the left" and the human's head is tilted to the left, the robot should say "Great job".

- When the human's head is not tilted to left after the robot started saying "Tilt your head to the left", the robot should say "Tilt your head to the right" again.

## Open-ended Q&A
<!--  / Interview -->

<!-- counting -->
<!-- order -->

Please implement and test a program that meets the below descriptions using [this online text editor](). Once you are done please submit your program using the Google Form, i.e., copy your program in the editor and paste it as an answer to the corresponding question in the Google Form.

- When the robot is ready, it should say "What does a typical day look like for you?".

- If the robot finished saying "What does a typical day look like for you?" and then the human started speaking and then the human stopped speaking, then the robot should say "What's your favorite way to waste time?"

- If the robot finished saying "What does a typical day look like for you?" and nothing happens for the next 3 seconds, the robot should say "What does a typical day look like for you?" again.

- If the robot finished saying "What's your favorite way to waste time?" and then the human started speaking and then the human stopped speaking, then the robot should say "All done"

- If the robot finished saying "What's your favorite way to waste time?" and nothing happens for the next 3 seconds, the robot should say "What's your favorite way to waste time?" again.

- When the robot finished saying the same question for 2nd times, it should say "Bye".
