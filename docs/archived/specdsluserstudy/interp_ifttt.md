# Robot Behavior Creation Tool User Study

The purpose of this study is to investigate the usability of a social robot behavior creation tool.

## Meet TaRo

<img src="./figs/taro1.jpg" height="150"/>
<img src="./figs/taro2.jpg" height="150"/>
<img src="./figs/taro3.jpg" height="150"/>

TaRo (stands for "Tablet Robot") is a robot face on a screen that can

- detect a human face
- detect the face direction of the human face
- sense voice activity

and

- display messages on screen
- speak via text-to-speech

## Robot Language

Image you can instruct TaRo do something by giving it instructions that only robots can understand--like a robot language!

## Robot Language Interpretation Tasks

Please examine instructions written in the robot language and describe what you think TaRo will do in plain English. _There is no right or wrong answer._

<!-- precedence: ! > && > || -->

### Interpretation Task 1

<!-- timeout -->

```
IF ready=true THEN setMessage="Hello"
IF 5s SINCE ready=true THEN setMessage="My name is TaRo"
IF 10s SINCE ready=true THEN setMessage="Nice to meet you"
IF 15s SINCE ready=true THEN setMessage="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdK0TOIr15w3NwO08Bo5sc-npOQen8wvCRUxW9GCZ-XaVby4A/viewform).

### Interpretation Task 2

<!-- loop -->

```
IF ready=true || sayFinished="Have a great day" THEN say="Hello there"
IF sayFinished="Hello there" THEN say="Have a great day"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdK0TOIr15w3NwO08Bo5sc-npOQen8wvCRUxW9GCZ-XaVby4A/viewform).

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' does nothing after 'robot.say="Rotate your head to the right"' -->

<!-- could be re-written with 4 IF ... THEN ... s -->
```
IF LAST ready=true
    || LAST ready=true && humanLookingAt=!"right"
  THEN say="Rotate your head to the right"
IF LAST ready=true && 3s SINCE humanLookingAt="right"
    || LAST humanLookingAt="right" && 3s SINCE ready=true
  THEN say="Great job"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdK0TOIr15w3NwO08Bo5sc-npOQen8wvCRUxW9GCZ-XaVby4A/viewform).

### Interpretation Task 4

<!-- counting -->

```
IF LAST ready=true THEN say="Rotate your head to the right"
IF LAST ready=true && 3rd humanLookingAt=!"right" THEN say="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdK0TOIr15w3NwO08Bo5sc-npOQen8wvCRUxW9GCZ-XaVby4A/viewform).

### Interpretation Task 5

<!-- order -->

```
IF LAST isHumanSpeaking=false && isHumanSpeaking=true THEN say="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdK0TOIr15w3NwO08Bo5sc-npOQen8wvCRUxW9GCZ-XaVby4A/viewform).

### Interpretation Task 6

<!-- combine -->

```
IF humanLookingAt="center" && isHumanSpeaking=false
    || LAST humanLookingAt="center" && isHumanSpeaking=false
    || LAST isHumanSpeaking=false && humanLookingAt="center"
  THEN say="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdK0TOIr15w3NwO08Bo5sc-npOQen8wvCRUxW9GCZ-XaVby4A/viewform).


## Post Study Questionnaire

Please answer the remaining questions in the Google Form.
