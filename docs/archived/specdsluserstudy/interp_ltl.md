# Robot Behavior Creation Tool User Study

The purpose of this study is to investigate the usability of a social robot behavior creation tool.

## Meet TaRo

<img src="./figs/taro1.jpg" height="150"/>
<img src="./figs/taro2.jpg" height="150"/>
<img src="./figs/taro3.jpg" height="150"/>

TaRo (stands for "Tablet Robot") is a robot face on a screen that can

- detect a human face
- detect the face direction of the human face
- sense voice activity

and

- display messages on screen
- speak via text-to-speech

## Robot Language

Image you can instruct TaRo do something by giving it instructions that only robots can understand--like a robot language!

## Robot Language Interpretation Tasks

Please examine instructions written in the robot language and describe what you think TaRo will do in plain English. _There is no right or wrong answer._

### Interpretation Task 1

<!-- timeout -->

```
IF ready=true THEN setMessage="Hello"
    && AFTER 5s setMessage="My name is TaRo"
    && AFTER 10s setMessage="Nice to meet you"
    && AFTER 15s setMessage="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdsUNDeEWEmT_96Qo_xGnD2X96ge4XcaRAep23FwEHp8QC1Rw/viewform).

### Interpretation Task 2

<!-- loop -->

```
IF ready=true || sayFinished="Have a great day"
  THEN say="Hello there"
      && EVENTUALLY IF sayFinished="Hello there" THEN say="Have a great day"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdsUNDeEWEmT_96Qo_xGnD2X96ge4XcaRAep23FwEHp8QC1Rw/viewform).

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' does nothing after 'robot.say="Rotate your head to the right"' -->

```
IF humanLookingAt=!"right"
  THEN EVENTUALLY IF ready=true THEN say="Rotate your head to the right"
      && EVENTUALLY IF humanLookingAt="right"
        THEN AFTER 3s say="Great job"
IF humanLookingAt="right"
  THEN EVENTUALLY IF ready=true THEN say="Rotate your head to the right" && AFTER 3s say="Great job"
IF ready=true THEN say="Rotate your head to the right"
    && EVENTUALLY IF humanLookingAt=!"right" THEN AFTER 3s say="Rotate your head to the right"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdsUNDeEWEmT_96Qo_xGnD2X96ge4XcaRAep23FwEHp8QC1Rw/viewform).

### Interpretation Task 4

<!-- counting -->

```
IF humanLookingAt=!"right"
  THEN EVENTUALLY ready=true
      && say="Rotate your head to the right"
      && EVENTUALLY IF humanLookingAt=!"right"
        THEN EVENTUALLY IF humanLookingAt=!"right"
          THEN EVENTUALLY IF humanLookingAt=!"right"
            THEN say="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdsUNDeEWEmT_96Qo_xGnD2X96ge4XcaRAep23FwEHp8QC1Rw/viewform).

### Interpretation Task 5

<!-- order -->

```
IF isHumanSpeaking=false THEN EVENTUALLY IF isHumanSpeaking=true THEN say="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdsUNDeEWEmT_96Qo_xGnD2X96ge4XcaRAep23FwEHp8QC1Rw/viewform).

### Interpretation Task 6

<!-- combine -->

```
IF humanLookingAt="center" && isHumanSpeaking=false THEN say="Hi"
IF humanLookingAt="center" THEN EVENTUALLY isHumanSpeaking=false && say="Hi"
IF isHumanSpeaking=false THEN EVENTUALLY humanLookingAt="center" && say="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdsUNDeEWEmT_96Qo_xGnD2X96ge4XcaRAep23FwEHp8QC1Rw/viewform).


## Post Study Questionnaire

Please answer the remaining questions in the Google Form.
