### Interpretation Task 1

<!-- timeout -->

```
sensor.ready=true
  robot.setMessage="Hello"
  AFTER 3s robot.setMessage="My name is TaRo"
  AFTER 6s robot.setMessage="Nice to meet you"
  AFTER 9s robot.setMessage="Bye"
```

Please write your answer in the Google Form.

### Interpretation Task 2

<!-- loop -->

```
sensor.ready=true
  robot.say="Hello there"
```

```
sensor.sayFinished="Hello there"
  robot.say="Have a great day"
```

```
sensor.sayFinished="Have a great day"
  robot.say="Hello there"
```

Please write your answer in the Google Form.

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' does nothing after 'robot.say="Rotate your head to the right"' -->

```
sensor.ready=true
  robot.say="Rotate your head to the right"
sensor.humanLookingAt="right"
  AFTER 3s robot.say="Great job"
```

```
sensor.humanLookingAt="right"
sensor.ready=true
  robot.say="Rotate your head to the right"
  AFTER 3s robot.say="Great job"
```

```
sensor.ready=true
  robot.say="Rotate your head to the right"
sensor.humanLookingAt="left"
  robot.say="Rotate your head to the right"
```

Please write your answer in the Google Form.

### Interpretation Task 4

<!-- counting -->

```
sensor.ready=true
  robot.say="Rotate your head to the right"
sensor.humanLookingAt=!"right"
  robot.say="Rotate your head to the right"
sensor.humanLookingAt=!"right"
  robot.say="Rotate your head to the right"
sensor.humanLookingAt=!"right"
  robot.say="Bye"
```

Please write your answer in the Google Form.

### Interpretation Task 5

<!-- order -->

```
sensor.isHumanSpeaking=true
sensor.isHumanSpeaking=false
  robot.say="Hi"
```

Please write your answer in the Google Form.

### Interpretation Task 6

<!-- combine -->

<!-- does not cover the case 'humanLookingAt' and 'isHumanSpeaking' emit false and "center" at the same time -->

```
sensor.humanLookingAt="center"
sensor.isHumanSpeaking=false
  robot.say="Hi"
```

```
sensor.isHumanSpeaking=false
sensor.humanLookingAt="center"
  robot.say="Hi"
```

Please write your answer in the Google Form.
