# Robot Behavior Creation Tool User Study

The purpose of this study is to investigate the usability of a social robot behavior creation tool.

## Meet TaRo

<img src="./figs/taro1.jpg" height="150"/>
<img src="./figs/taro2.jpg" height="150"/>
<img src="./figs/taro3.jpg" height="150"/>

TaRo (stands for "Tablet Robot") is a robot face on a screen that can

- detect a human face
- detect the face direction of the human face
- sense voice activity

and

- display messages on screen
- speak via text-to-speech

## Robot Language

Image you can instruct TaRo do something by giving it instructions that only robots can understand--like a robot language!

## Robot Language Interpretation Tasks

Please examine instructions written in the robot language and describe what you think TaRo will do in plain English. _There is no right or wrong answer._

<!-- '-' means 1 sec -->
<!-- 'sensor' emits state chances except sayFinished -->
<!-- 'robot' emits events -->

### Interpretation Task 1

<!-- timeout -->

```
sensor.ready    : f-t-----------
robot.setMessage: --a--b--c--d--

a="Hello"
b="My name is TaRo"
c="Nice to meet you"
d="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).

### Interpretation Task 2

<!-- loop -->

```
sensor.ready      : f-t------------
sensor.sayFinished: -----a---b--a--
robot.say         : --a--b---a--b--

a="Hello there"
b="Have a great day"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' starting with !"right" and emits no more state change events -->

```
sensor.ready         : f-t-------
sensor.humanLookingAt: n----r----
robot.say            : --a-----b-

n=!"right"
r="right"
a="Rotate your head to the right"
b="Great job"
```

```
sensor.ready         : f-t-------
sensor.humanLookingAt: r---------
robot.say            : --a--b----

r="right"
a="Rotate your head to the right"
b="Great job"
```

```
sensor.ready         : f-t-------
sensor.humanLookingAt: n------l--
robot.say            : --a----a--

n=!"right" && !"left"
l="left"
a="Rotate your head to the right"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).

### Interpretation Task 4

<!-- counting -->

<!-- does not cover (i) the case 'humanLookingAt' starting with "right" and (ii) the case 'humanLookingAt' emits "right" after ready=true -->

```
sensor.ready         : f-t----------------
sensor.humanLookingAt: i-----j--k-----l---
robot.say            : --a---a--a-----b---

i=!"right"
j=!"right"
k=!"right"
l=!"right"
a="Rotate your head to the right"
b="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).

### Interpretation Task 5

<!-- order -->

```
sensor.isHumanSpeaking: f--t---f-----
robot.say             : -------a-----

a="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).

### Interpretation Task 6

<!-- combine -->

```
sensor.humanLookingAt : n--c---------
sensor.isHumanSpeaking: f------------
robot.say             : ---a---------

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n-----c-----
sensor.isHumanSpeaking: f----t---f--
robot.say             : ---------a--

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n--------c--
sensor.isHumanSpeaking: f--t---f----
robot.say             : ---------a--

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n------c----
sensor.isHumanSpeaking: f--t---f----
robot.say             : -------a----

n=!"center"
c="center"
a="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).


## Post Study Questionnaire

Please answer the remaining questions in the Google Form.
