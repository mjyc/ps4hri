# CHI 2020

Submission deadline 9/20

## Contributions

We present a means of authoring reactive programs using synthesis and an iterative, human-in-the-loop workflow for specification refinement. We demonstrate the expressiveness of our method by synthesizing HRI applications using analog inputs such as video and speech. We show that users are capable of writing the specifications necessary to define full applications without common issues such as concurrency bugs.

## Evaluation

### Stress-test synthesis

Show the limits of our synthesis algorithm by measuring the performance of microbenchmarks (i.e., can synthesize programs of up to X instructions over inputs of Y length in up to Z seconds)

### User study

Hypothesis: given an HRI application to author, users complete the task faster and with fewer errors with the tool than writing code by hand.

Through observation and survey, find out if users find writing specifications easy/difficult, if there are particular trouble areas, etc.

## Discussion

**Intended users** Does the tool empower novice programmers to author programs? Does it help programmers to write bug-free concurrent code?

**Interpretability** Ideally, the tool should not be like a compiler where the user never looks at the output, but like a collaboration between the user and the synthesizer. The user should be able to read and understand the code. 

## TODOs

+ Define tasks for user study
+ IRB/other logistics for performing user study. Can we be ready to begin the study in mid August?
+ Finalize DSL of target synthesis language
+ Tune synthesis performance (each user interaction should return in ~5s)
+ Finalize the syntax of the specifications
+ Create GUI for user study (run in browser, text-based, elicit PBD through ASCII marble diagrams)
+ Run evaluation on microbenchmarks for performance testing