# Tasks & Specs

## Social Robot

* [Previous project brief](../archived/program_synthesis_for_social_robots/project_brief.md)
  * [specs](../archived/program_synthesis_for_social_robots/specs.md)
  * [algorithm sketch](../archived/program_synthesis_for_social_robots/algorithm_sketch.md)
* [Social robot specifications](https://docs.google.com/spreadsheets/d/1ECOZ6Omf_0KySbDgeJeevOb0jDPab6WzvvTaH3GLdG0/edit#gid=0)
  * [Specifications and Example Scenarios](https://docs.google.com/document/d/1EzTdj88gURl06LcBHEoqSs_qiz3LX361v2lsMQqCCAA/edit)


## Mobile Robot

* Relay tasks
  * [specs](../archived/eup/robot_tasks/relay/delivery.md)
  * [demo](../archived/eup/robot_tasks/relay/demo.md)
  * [mingle](../archived/eup/robot_tasks/relay/mingle.md)
  * [tour](../archived/eup/robot_tasks/relay/tour.md)
* Information gathering tasks
  * checking: go to a place and take check the condition
  * searching: go to places until it finds the requested thing
  * monitoring: alert if the requested thing/condition appears
  * summary: summarize requested / salient events from the past

* Specs
  * always stop navigating when a person taps the robot
  * go back to the dock when battery is < 10%
    * do not engage with humans when going back to the dock
  * always ask "Do you need anything else" before leaving a guest
  * if a guest is 10ft away then smile and if a guest is less than 10ft away then ask if the guest needs anything
