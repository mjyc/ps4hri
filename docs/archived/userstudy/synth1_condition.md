In this study, you will be asked to program interactive robot behaviors using a program synthesizer.

The program synthesizer allows you to program interactive robot behaviors by specifying specs.
