---
breaks: false
---

# Study1 Instructions

In this study, you will be asked to program interactive robot behaviors using JavaScript with a reactive programming library.

## Consent Form

Please read the study [consent form](https://docs.google.com/document/d/1r_pJDJzm9-NRgYDiYzyTaZljc3U-ES6vwkqzBkCw47w/edit?usp=sharing).

## **IMPORTANT!** Study Feasibility Test

Open [this link](https://codesandbox.io/embed/example-n78vnx5zwm?expanddevtools=1?fontsize=14&module=%2Fsrc%2Fmain.js) in a Chrome or Firefox browser and see if it can

1. detect your face: check the value of **isFaceVisible** is changing as expected by showing and hiding your face from the camera (scroll down to see the live camera feed).
2. detect the direction of your face: check the value of **faceLookingAt** is changing as expected by turning your head left and right.
3. detect your voice activity: check the value of **isHumanSpeaking** is changing as expected by speaking and stopping.

_If **isFaceVisible** or **faceLookingAt** is not working,_

- make sure a camera is connected and properly setup, e.g., try [camera test](https://www.onlinemictest.com/webcam-test/)
- try changing the lightening condition of the room or changing the camera angle

_If **isHumanSpeaking** is not working (e.g., always set to `true` or `false`),_

- make sure a microphone is connected and properly setup, e.g., try this [microphone test](https://www.onlinemictest.com/)
- try adjusting the microphone input level (see "Tried the steps and it still doesn't work? Check the settings in your operating system:" section in the [microphone test](https://www.onlinemictest.com/))

**IMPORTANT!** If you cannot get the detectors to work well enough for you, please _do not participate in this study_.

## The TabletFace Robot

The TabletFace robot is a simplified, interactive social robot on browser that can be programmed to:

- detect a human face (camera required)
- detect human voice activities (microphone required)
- display messages on screen
- speak via text-to-speech (speaker required)

Note that the robot _does not recognize faces nor recognize speech_.

## Reactive Programming

The robot's behavior is represented as a [JavaScript function](https://www.w3schools.com/js/js_functions.asp) that takes a set of sensor _streams_ as an input variable and returns a set of action _streams_ as an output variable.
For example, the following function will say "true" whenever the robot detects a human face and "false" otherwise:

```js
function main(sensors) {
  var actions = {
    setMessage: sensors.isFaceVisible
  };
  return actions;
}
```

Here is the full list of available input sensors streams:

- `ready`: emits `true` when the robot becomes ready, i.e., when the human taps the "Tap to start" button.
- `isFaceVisible`: emits `true` if a human face appears in the camera view and `false` otherwise; emits `false` initially
- `faceLookingAt`: emits `"left"` if the detected human face starts looking at the left side (from the human's perspective), `"center"` if it starts looking at the center, `"right"` if it starts looking at the right side, and an empty string `""` if the human face is not visible; emits `""` initially
- `isHumanSpeaking`: emits `true` if a voice activity gets detected and `false` otherwise; emits `false` initially
- `sayFinished`: emits a string the robot has finished saying when the robot does so; emits `false` initially

And here is the full list available output action streams:

- `setMessage`: emits a boolean, number, or string that will be displayed on screen; ignores `null` and `undefined`
- `say`: emits a boolean, number, or string that will be spoken by the robot; ignores `null` and `undefined`

### Marble Diagram

A stream represents a variable that can change its value over time.
We use ASCII marble diagrams to represent streams.

As an illustrative example, let's say there is a robot program that displays `1` and says `"hello"` whenever a human face becomes visible and displays `0` and says "goodbye" otherwise.
Possible input and output traces of such program could be visualized using marble diagrams:

```
sensors.isFaceVisible:  f--t---f-t----
actions.setMessage:     0--1---0-1----
actions.say:            g--h---g-h----

h="hello"
g="goodbye"
```

where

- `-` the duration of 1 second without having any value
- `t` the boolean value true
- `f` the boolean value false
- `1` numbers 0-9 are treated as literal numeric values
- `a` other literal values are strings or variable; using a variable requires variable definition, e.g., `h="hello"` for using a variable `h`
- horizontal whitespace is ignored, and can be used to help vertically align multiple marble diagrams

### Example Program

In reactive programming, desired behaviors are expressed as chaining [stream operators](#Stream-Operators) to input streams or intermediate streams until one get desired output action streams.

Here is a robot program that displays the number of faces the robot has seen so far:

```js
function main(sensors) {
  var numVisibleFaces = sensors.isFaceVisible.map(function(b) {
    if (b) {
      return 1;
    } else {
      return 0;
    }
  });
  var actions = {
    setMessage: numVisibleFaces.scan(function(acc, num) {
      return acc + num;
    }, 0)
  };
  return actions;
}
```

Check out [the robot running this program](https://codesandbox.io/embed/example-n78vnx5zwm?expanddevtools=1?fontsize=14&module=%2Fsrc%2Fmain.js); see if you can move away and come back into the camera's field of view to increase the count.

Here are marble diagrams representing possible input, output, and intermediate streams:

```
sensors.isFaceVisible:  f--t--f-----t-------f---
numVisibleFaces:        0--1--0-----1-------0---
actions.setMessage:     0--1--1-----2-------2---
```

### Stream Operators

Here is the list of available stream operators:

- [`map`](#mapproject)
- [`mapTo`](#mapToprojectedValue)
- [`filter`](#filterpasses)
- [`take`](#takeamount)
- [`scan`](#scanaccumulate-seed)
- [`merge`](#mergestream1-stream2)
- [`combineLatest`](#combineLateststream1-stream2)
- [`bufferCount`](#bufferCountbufferSize)
- [`delay`](#delayperiod)
- [`debug`](#debugspy)
  <!-- * [`debounce`](#debounceperiod) -->
  <!-- * [`startWith`](#startwithinitial) -->

#### `map(project)`

Transforms each value from the input stream through a `project` function, to get a stream that emits those transformed values.

Example code:

```js
var out = in.map(function(i) { return i * 2; });
```

Marble diagram:

```
in:  --0------1------2---------3-----4-----
out: --0------2------4---------6-----8-----
```

#### `mapTo(projectedValue)`

It's like `map`, but transforms each input value to always the same
constant value on the output Stream.

Example code:

```js
var out = in.mapTo(1);
```

Marble diagram:

```
in:  --1---3--5-----7-----
out: --1---1--1-----1-----
```

#### Arguments:

- `projectedValue` A value to emit on the output Stream whenever the input Stream emits any value.

#### Returns: Stream

#### `filter(passes)`

Only allows values that pass the test given by the `passes` argument.

Each value from the input stream is given to the `passes` function. If the function returns `true`, the value is forwarded to the output stream, otherwise it is ignored and not forwarded.

Example code:

```js
var out = in.filter(function(i) { return i % 2 === 0; });
```

Marble diagram:

```
in:  --1---2--3-----4-----5--------6--7------8------
out: ------2--------4--------------6---------8------
```

#### `take(amount)`

Lets the first amount many values from the input stream pass to the output stream, then makes the output stream complete.

Example code:

```js
var out = in.take(3);
```

Marble diagram:

```
in:  --a---b--c----d---e--
out: --a---b--c-----------
```

#### `scan(accumulate, seed)`

"Reduces" the stream onto itself.

Combines values from the past throughout the entire execution of the input stream, allowing you to accumulate them together. It's essentially like `Array.prototype.reduce`.

The output stream starts by emitting the `seed` which you give as an argument.
Then, when a value happens on the input stream, it is combined with that
seed value through the `accumulate` function, and the output value is
emitted on the output stream.
`scan` remembers that output value as `acc` ("accumulator"), and then when a new input value `x` happens, `acc` will be combined with that to produce the new `acc` and so forth.

Example code:

```js
var out = in.fold(function (acc, x) { return acc + x; }, 3);
```

Marble diagram:

```
in:  ------1-----1--2----1----1------
out: 3-----4-----5--7----8----9------
```

#### `merge(stream1, stream2)`

Blends multiple streams together, emitting values from all of them
concurrently.

_merge_ takes multiple streams as arguments, and creates a stream that
behaves like each of the argument streams, in parallel.

Example code:

```js
var out = merge(in1, in2);
```

Marble diagram:

```
in1: --1----2-----3--------4---
in2: ----a-----b----c---d------
out: --1-a--2--b--3-c---d--4---
```

#### `combineLatest(stream1, stream2)`

Combines multiple input streams together to return a stream whose values
are arrays that collect the latest events from each input stream.

_combineLatest_ internally remembers the most recent value from each of the input streams.
When any of the input streams emits an event, that event together
with all the other saved events are combined into an array.
That array will be emitted on the output stream.
It's essentially a way of joining together the events from multiple streams.

Example code:

```js
var out = combineLatest(in1, in2);
```

Marble diagram:

```
in1: --1----2-----3--------4---
in2: ----a-----b-----c--d------
out: ----e--f--g--h--i--j--k---

e=[1,a]
f=[2,a]
g=[2,b]
h=[3.b]
i=[3,c]
j=[3,d]
k=[4,d]
```

#### `bufferCount(bufferSize)`

Buffers the input stream values until the size hits the `bufferSize`.

Example code:

```js
var out = in.bufferCount(3);
```

Marble diagram:

```
in:  ---1---2-----3-----4-----5--------
out: -------------a-----b-----c--------

a=[1,2,3]
b=[2,3,4]
c=[3,4,5]
```

#### `delay(period)`

Delays emitted values by a given time period.

Example code:

```js
var out = in.delay(3);
```

Marble diagram:

```
in : 1----2--3--4----5----
out: ---1----2--3--4----5-
```

#### `debug(spy)`

Returns an output stream that identically behaves like the input stream, but also runs a spy function for each event, to help you debug your app.

debug takes a spy function as argument, and runs that for each event happening on the input stream. If you don't provide the spy argument, then debug will just console.log each event. This helps you to understand the flow of events through some operator chain.

Example code:

```js
var out = in.debug();
```

Marble diagram:

```
in:  --1----2-----3-----4--
out: --1----2-----3-----4--
```

<!-- #### `debounce(period)`

Delays values until a certain amount of silence has passed. If that timespan of silence is not met the value is dropped.

Example code:

```js
var out = in.debounce(3);
```

Marble diagram:

```
in:  --1----2--3--4----5-
out: -----1----------4---
``` -->

<!-- #### startWith(initial)

Prepends the given initial value to the sequence of events emitted by the input stream.

Example code:

```
var out = in.startWith(0);
```

Marble diagram:

```
in:  ---1---2-----3---
out: 0--1---2-----3---
``` -->

## Task 0: Practice Task

Please practice implementing and testing a program that meets the below descriptions using [this online code editor](https://codesandbox.io/embed/sandbox-10on7qrq4?expanddevtools=1?fontsize=14&module=%2Fsrc%2Fmain.js).
**You do not need to submit your program for this task.** It is a practice task; the solution is available below.

- When the robot is ready, it should say "Hello"; e.g.,

  ```
  sensors.ready: -----t---
  actions.say:   -----a---

  a="Hello"
  ```

- When the robot finished saying "Hello", it should say "My name is TabletFace"; e.g.,

  ```
  sensors.sayFinished: ----a----
  actions.say:         ----b----

  a="Hello"
  b="My name is TabletFace"
  ```

- When the robot finished saying "My name is TabletFace", it should say "Nice to meet you"; e.g.,

  ```
  sensors.sayFinished: ----a----
  actions.say:         ----b----

  a="My name is TabletFace"
  b="Nice to meet you"
  ```

#### Hints

Try using, but not limited to, the following operators: [`mapTo`](#maptoprojectedvalue), [`map`](#mapproject), and [`merge`](#mergestream1-stream2).

#### Solution

```js
function main(sensors) {
  var say = merge(
    sensors.ready.mapTo("Hello"),
    sensors.sayFinished.debug().map(function(result) {
      if (result === "Hello") {
        return "My name is TabletFace";
      } else if (result === "My name is TabletFace") {
        return "Nice to meet you";
      }
    })
  );

  var actions = {
    say: say
  };
  return actions;
}
```

Note that using [`debug`](#debugspy) was not necessary. It was merely used to demonstrate how it can be used for debugging.

## Task 1: Instruction

Please implement and test a program that meets the below descriptions using [this online code editor](https://codesandbox.io/embed/sandbox-10on7qrq4?expanddevtools=1?fontsize=14&module=%2Fsrc%2Fmain.js).
Once you are done please **submit your program using Google Form**, i.e., copy and paste your program in main.js as an answer to the corresponding question.

- When the robot is ready, it should say "Let's start from looking forward"; e.g.,

  ```
  sensors.ready: -----t---
  actions.say:   -----a---

  a="Let's start from looking forward"
  ```

- When the robot is finished saying the first instruction, it should say "and now slowly rotate to your right"; e.g.,

  ```
  sensors.sayFinished:   ----a----
  actions.say:           ----b----

  a="Let's start from looking forward"
  b="and now slowly rotate to your right"
  ```

- When the robot finished saying the second instruction and the human face is looking at the right, the robot should say "and now slowly rotate to your left"; e.g.,

  ```
  actions.sayFinished:   ---b-----
  sensors.faceLookingAt: -----r---
  actions.say:           -----c---

  b="and now slowly rotate to your right"
  r="right"
  c="and now slowly rotate to your left"
  ```

- When the robot finished saying the third instruction and the human face is looking at the left, the robot should say "You are done!"; e.g.,

  ```
  actions.sayFinished:    -----c---
  sensors.faceLookingAt:  ---l-----
  actions.say:            -----d---

  c="and now slowly rotate to your left"
  l="left"
  d="Great job!"
  ```

#### Hints

Try using, but not limited to, the following operators: [`mapTo`](#maptoprojectedvalue), [`filter`](#filterpasses), [`take`](#takeamount), [`merge`](#mergestream1-stream2), and [`combineLatest`](#combineLateststream1-stream2).

## Task 2: Storytelling

Please implement and test a program that meets the below descriptions using [this online code editor](https://codesandbox.io/embed/sandbox-10on7qrq4?expanddevtools=1?fontsize=14&module=%2Fsrc%2Fmain.js).
Once you are done please **submit your program using Google Form**, i.e., copy and paste your program in main.js as an answer to the corresponding question.

- When the robot is ready, it should say "Brown bear, brown bear, what do you see?"; e.g.,

  ```
  sensors.ready: ----t----
  actions.say:   ----a----

  a="Brown bear, brown bear, what do you see?"
  ```

- If it has been at least 1 seconds since the robot finished saying the first sentence and the human is not speaking currently, then the robot should say "I see a human looking at me."; e.g.,

  ```
  sensors.isHumanSpeaking: f-------
  sensors.sayFinished:     --a-----
  actions.say:             -----b--

  a="Brown bear, brown bear, what do you see?"
  b="I see a human looking at me."
  ```

- If it has been at least 1 seconds since the robot finished saying the second sentence and the human is not speaking currently, then the robot should say "The END"; e.g.,

  ```
  sensors.isHumanSpeaking: --t----f--
  sensors.sayFinished:     -b--------
  actions.say:             -------c--

  b="I see a human looking at me."
  c="The END"
  ```

<!--
  snesors.faceLookingAt:   c-------

  c="center"
  -->

<!--
- If the robot has not seen the human for at least 5 seconds, it should not say anything; e.g.,

  ```
  sensors.isFaceVisible:   -f--------
  sensors.isHumanSpeaking: f---------
  sensors.sayFinished:     ------a---
  actions.say:             ----------
  ```
-->

#### Hints

Try using, but not limited to, the following operators: [`mapTo`](#maptoprojectedvalue), [`filter`](#filterpasses), [`delay`](#delayperiod), [`merge`](#mergestream1-stream2), and [`combineLatest`](#combineLateststream1-stream2).

## Task 3: Interview

Please implement and test a program that meets the below descriptions using [this online code editor](https://codesandbox.io/embed/sandbox-10on7qrq4?expanddevtools=1?fontsize=14&module=%2Fsrc%2Fmain.js).
Once you are done please **submit your program using Google Form**, i.e., copy and paste your program in main.js as an answer to the corresponding question.

- When the robot is ready, it should say "What does a typical day look like for you?"; e.g.,

  ```
  sensors.ready: ----t---
  actions.say:   ----a---

  a="What does a typical day look like for you?"
  ```

- If the robot finished saying the first question and then the human started speaking and then the human stopped speaking, then the robot should say "What odd talent do you have?"; e.g.,

  ```
  sensors.sayFinished:     --a-------
  sensors.isHumanSpeaking: ---t-f----
  actions.say:             -----b----

  a="What does a typical day look like for you?"
  b="What odd talent do you have?"
  ```

- If the robot finished saying the second question and then the human started speaking and then the human stopped speaking, then the robot should say "What's the most spontaneous thing you've done?"; e.g.,

  ```
  sensors.sayFinished:     --b------------
  sensors.isHumanSpeaking: -t-f----t--f---
  actions.say:             -----------c---

  b="What odd talent do you have?"
  c="What's the most spontaneous thing you've done?"
  ```

<!--
- If the human did not respond to the robot more than 5 seconds then the robot should repeat saying the question.

  Expected input and output streams as marble diagrams:

  ```
  sensors.sayFinished:     ---a-----------
  sensors.isHumanSpeaking: f--------------
  sensors.say:             -----------a---

  a="What does a typical day look like for you?"
  ```
-->

#### Hints

Try using, but not limited to, the following operators: [`mapTo`](#maptoprojectedvalue), [`merge`](#mergestream1-stream2), and [`bufferCount`](#bufferCountbufferSize).

## Next

Once you submitted all programs, follow instructions in Google Form to proceed with the post-study questionnaire.
