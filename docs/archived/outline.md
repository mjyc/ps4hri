# Title

Intuitive Specification Languages for \\ Synthesis of Interactive Robot Programs


# Contributions


# Introduction


# Related Work


# Natural Specifications


# Interactive Robot Programs

## FRP

## Robot API


# Specification Languages \& Program Synthesis

## Spec language expressivity

## Program Synthesis from Specs


# Program Spec Intuitiveness

## User interface

## Online study

## In-person study
