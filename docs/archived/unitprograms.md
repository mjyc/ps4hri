### Timeout Sequence

```
sensor.ready    : t------
robot.setMessage: 1--2-3-
```

```
sensor.ready    : f-t------
robot.setMessage: --a-b--c-

a="hello"
b="nice to meet you"
c="bye"
```

### Action Sequence

```
sensor.ready      : t-------
sensor.sayFinished: --1-2-3-
robot.say         : 1-2-3-4-
```

```
sensor.ready      : f-t------
sensor.sayFinished: ----a-b-c
robot.say         : --a-b-c--

a="hello"
b="nice to meet you"
c="bye"
```

### Wait Sequence

```
sensor.ready   : t------
sensor.waitDone: --21--3
robot.wait     : 2-13---
```

<!-- TODO: update the rest -->

### Interpretation Task 3

<!-- branching -->

<!-- does not cover the case 'humanLookingAt' starting with !"right" and emits no more state change events -->

```
sensor.ready         : f-t-------
sensor.humanLookingAt: n----r----
robot.say            : --a-----b-

n=!"right"
r="right"
a="Rotate your head to the right"
b="Great job"
```

```
sensor.ready         : f-t-------
sensor.humanLookingAt: r---------
robot.say            : --a--b----

r="right"
a="Rotate your head to the right"
b="Great job"
```

```
sensor.ready         : f-t-------
sensor.humanLookingAt: n------l--
robot.say            : --a----a--

n=!"right" && !"left"
l="left"
a="Rotate your head to the right"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).

### Interpretation Task 4

<!-- counting -->

<!-- does not cover (i) the case 'humanLookingAt' starting with "right" and (ii) the case 'humanLookingAt' emits "right" after ready=true -->

```
sensor.ready         : f-t----------------
sensor.humanLookingAt: i-----j--k-----l---
robot.say            : --a---a--a-----b---

i=!"right"
j=!"right"
k=!"right"
l=!"right"
a="Rotate your head to the right"
b="Bye"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).

### Interpretation Task 5

<!-- order -->

```
sensor.isHumanSpeaking: f--t---f-----
robot.say             : -------a-----

a="Hi"
```

Please write your answer in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSebonlMGbKFvhHRFprclefHEhYaSISCEt-ri4M36tkK7GA9Aw/viewform).

### Interpretation Task 6

<!-- combine -->

```
sensor.humanLookingAt : n--c---------
sensor.isHumanSpeaking: f------------
robot.say             : ---a---------

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n-----c-----
sensor.isHumanSpeaking: f----t---f--
robot.say             : ---------a--

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n--------c--
sensor.isHumanSpeaking: f--t---f----
robot.say             : ---------a--

n=!"center"
c="center"
a="Hi"
```

```
sensor.humanLookingAt : n------c----
sensor.isHumanSpeaking: f--t---f----
robot.say             : -------a----

n=!"center"
c="center"
a="Hi"
```
