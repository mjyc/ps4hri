# 2020.2.24

## Related Work

- Robot Programming by Non-Experts: Intuitiveness and Robustness of One-Shot Robot Programming
- RAZER
- Intuitive Task-Level Programming by Demonstration Through Semantic Skill Recognition
- Towards Intuitive Robot Programming Using Finite State Automata



# 2020.1.23

## Goals for 1/27 meeting

- sequence demo
  - show my way of resolving an ambiguity, e.g., wait or not, loop or not
  - show differences between marble, evsc, ifttt
    - w.r.t. use of symbolic variables & angexe vs. synth
  - show admin spec use case (technical)
  - show env use case (technical)
- reactive demo
  - show my way of resolving an ambiguity, e.g., loop or not, hierarchy or not

- ambiguity resolution strategies (a part of the "synthesis method")
  1. pick & visualize
  1. interactive

- anticipated issues with spec DSLs
  - marble diagram
    - memorize things and does not generalize
  - EVSC & IFTTT
    - easier to being in conflict

### Updated on 1.24

- Meeting outline
  - The Project Goal: a study of specs DSLs
  - The Week 4 Goal
    - check the synthesis feasibility using all 3 spec DSLs
      - starting with a few tasks
  - The Week 4 Results
    - storytelling & neck exercise synthesis feasibility check
    - highlights:
      - handling missing common sense via admin-specs
        - could also be framed as "ability to enforce the robo-admin specs"
        - useful for specs that are less informative/has more symbolic variables/looks more like programs/looks less like demonstrations
        - but sometimes causes a conflict with the robot-user specs
        - **admin-spec** is one ingredient to make synthesis output generalizable
      - detect loops
        - via **the structure of the hole**, which is another ingredient to make synthesis output generalizable
      - HFSM
        - makes synthesis scalable
        - makes synthesis output interpretable
      - Query
        - and not optimization


## Contributions

- Comparison of Spec DSLs
  - typical spec DSLs
  - empirically and theoretically evaluate intuitiveness vs. expressiveness
    - empirical results
      - "interpret-ability" user study
      - "author-ability" user study

- Motivation
  - spec DSLs are the interfaces for a synthesizer, need to know which one is useful
  - a confounding factor: the "synthesis method" influences UX via computation time and quality
  - why HRI program synthesis?
    - a nice mean for many stake holders such as a robot owner, robot users, and a programmer, to work together

- Technical
  - Demonstrate the techniques for making FRP synth possible for HRI programs
    - share details, e.g., the use of
      - admin spec
      - environment spec
    - claim: achieved the feasibility / a novel synthesis application


## Outline

- Spec DSLs
- Synthesizer
  - share technical details
- Spec Formal comparions
- Spec Interpretability Study
- Spec Authorability Study
- Analysis and conclusion

### Study design resources

- https://elifesciences.org/articles/48175
- https://www.itl.nist.gov/div898/handbook/
- https://methods.sagepub.com/reference/encyclopedia-of-survey-research-methods
- https://opentextbc.ca/researchmethods/


# 2020.1.21

## Contributions

- User study
  - which spec language makes sense? and why?
    - demonstration
    - trigger action
    - hybrid?
  - which synthesizer makes sense? and why?
    - one-shot vs. iterative

- Technical
  - scalability? (speed)
  - expressivity? (LTL or FSM)


  - at high-level
    - an approach that allows a user to synthesize HRI programs
      - scalable?
      - fast?
    - measures
      - expressivity
        - benchmark tasks (based on BT?)
      - ease-of-use?
        -
  - program sketch
    - FSM
    - FRP
  - hri program synth
    - FSM
    - FRP
  - human model



# 2020.1.19

## Research question

- Intuitive Specification Languages for \\ Synthesis of Interactive Robot Programs
  - demonstration?
  - conditions?
  - behavior tree?
