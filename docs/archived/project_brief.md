# Title

End-user Programming for Interactive Robots via Program Synthesis

(End-user) Programming for Interactive Robots: Program Synthesis Approach

# NABC

- Need: To empower end-users, we need to make end-users to create programs easily
- Approach: Let's use program synthesis
- Benefits: It generates programs that engineers can work with directly & can verify programmer-updated programs w.r.t. specs
- Competition: We could give end-users GUI

# Contributions

- hands over difficult interactive robot programming problems to synthesizer

  - concurrency, esp., coordination, timing, etc.
  - error handling
  - structuring programs

- evaluation

  - saves time
  - generates useful/interpretable programs
  - ease-of-use (might be hard to measure)
  - case study? iteration between end-user & programmer

- technical contributions
  - building a synthesizer that can handle LTL or traces specs
  - a synthesizer that takes an "environment model"
  - a synthesizer that can produces reasonable programs, e.g., FSM transition functions or FRP programs

## Reminders

- programs that are hard to write in ROS is great
- programs that have properties that makes them difficult, e.g., coordinating complex triggers & actions, adjusting timing, tuning thresholds, etc.

# Related Work

See FSM synth project brief's [Related Work section](./project_brief_fsmsynth.md#related-work)

# Methods

- social norms and system designer specs as traces, invariant, or LTL specs
- interaction traces, invariants (for reactive specs) or LTL rules (for procedural) as specs

## Tasks and Specs

See [tasks_and_specs.md](./tasks_and_specs.md)

# Evaluation

- algorithm analysis - synthesis time across different programs, specs
- demonstrate ability to synthesize multiple programs
- case study - iteration between a programmer & non programmer
