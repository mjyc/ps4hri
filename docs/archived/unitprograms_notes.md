1. Branch
    - e.g., neck exercise inputs
    - marb spec
      - two-paths demonstration
    - evsc spec
      - two-paths  demonstration with unspecified time
    - ifttt spec
      - full spec

2. Sequence - looping-event-dependent
    - e.g., monologue
    - marb spec
      - path demonstration
      - _potentially with missing looping-events_
    - evsc spec
      - path demonstration
      - _potentially with missing looping-events_
    - ifttt spec
      - full spec
    - potential challenges
      - how do you...

3. Loop
    - e.g., marquee (with timeout?)

4. counting
    - e.g., marquee and end
    - solution ideas:
      - counting operator

5. combine

6. order

7. {what_maya_requested}
