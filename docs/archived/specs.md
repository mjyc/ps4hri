## Test specs

Notations:

a -> b: if a then b
X: next
G: always
F: eventually

### Task 0

- ready AND say="hello"
- sayFinished="hello" AND say="My name is TabletFace"
- sayFinished="My name is TabletFace" AND say="Nice to meet you"
- say=x -> X G NOT say // shouldn't say the same thing twice

### Task 1: Instructions

Variables:

a="Let's start from looking forward"
b="and now slowly rotate to your right"
c="and now slowly rotate to your left"
d="Great job!"
r="right"
l="left"

Using LTL:

- ready AND say=a
- sayFinished=a AND say=b
- (sayFinished=b -> G faceLookingAt=l OR faceLookingAt=l -> G sayFinished=b) AND say=c
- {shouldn't say the same thing twice}

Using the new ordering spec DSL:

- sF=b
- fL=r // assume EVENTUALLY for sensors
- say=c // assume AND for actions

OR

- fL=r
- sF=b
- say=c

where sF=sayFinished, fL=faceLookingAt

### Task 2:

### Task 3: Interview

Variables:

a="What does a typical day look like for you?"
b="What odd talent do you have?"
c="What's the most spontaneous thing you've done?"

Using the new ordering spec DSL:

- sF=a
- HS=t // assume EVENTUALLY for sensors
- HS=f // assume EVENTUALLY for sensors
- say=c // assume AND for actions

OR

- sF=a
- HS=f // assume EVENTUALLY for sensors
- HS=t // assume EVENTUALLY for sensors
- HS=f // assume EVENTUALLY for sensors
- say=c // assume AND for actions

where sF=sayFinished, HS=isHumanSpeaking

the "If the robot has not seen the human for at least 5 seconds, it should not say anything" spec:

(should be ANDed with others)

- FV=f for 5s (or - wait/sleep for 5s, after 5s) // debounce
- sf=x OR HS=f
- don't say=y // create stop?

where FV=isFaceVisible
