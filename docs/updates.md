# Week 9

## Progress Update

- deep dive into experimenting with using three different specs
  1. demonstrations
  2. ifttt (rules)
  3. event script
  - see the "Details" section below
  - **NOTE** for each, show (i) the target programs (machine), (ii) the output machine and (iii) explain challenges

- read the new NRI grant paper and [autotap](https://people.cs.uchicago.edu/~shanlu/paper/autotap.pdf) paper
  - motivations: I was looking for (i) an answer to "why synthesis?" and (ii) inspiring, convincing, applicable tasks

- plan for the this/next week
  - define a few concrete problems that I can solve (based on the readings); that I expect the solver to find and start making some progress towards implementing it

## Details

- the target program
  ```graphviz
  digraph G {
  ready -> say_hello [ label = "(and (equal? faceVis #t) (equal? start #t))" ]
  say_hello -> say_nice [ label = "(and (equal? faceVis #t) (equal? sayDone hello))" ]
  say_nice -> say_bye [ label = "(and (equal? faceVis #t) (equal? sayDone nice to meet you))" ]
  }
  ```

- demonstration spec synth output
  ```graphviz
  digraph G {
  say_nice -> say_bye [ label = "(and (equal? start #t) (equal? sayDone nice to meet you))" ]
  ready -> say_hello [ label = "(and (equal? faceVis #t) (equal? start #t))" ]
  say_hello -> say_nice [ label = "(equal? sayDone hello)" ]
  }
  ```
  - **observation #1: unnecessary and missing transition conditions**

- ifttt spec synth
  ```graphviz
  digraph G {
  ready -> say_bye [ label = "(and (equal? start #t) (equal? faceVis #t))" ]
  say_bye -> say_bye [ label = "(equal? start #t)" ]
  ready -> say_bye [ label = "(and (equal? faceVis #t) (equal? sayDone nice to meet you))" ]
  }
  ```
  - (outdated) writing specs that forces synthesizer to discover a non-empty program is not trivial
    - **the problem:** the synthesizer finds "any" program
    - **hypothesis #1:** using the way I convert ifttt specs into ltl specs, the synthesizer finds an empty program because the `if` condition part of the ltl specs allows the empty program to be correct
  - solution ideas:
    - add "program cannot be empty" spec or look into forcing the synthesizer to choose "one" program
    - update my method for converting ifttt specs into ltl specs; the `if` statement problem mentioned in hypothesis #1 may persist, also it's not trivial to make intuitive ltl specs (or non-ltl ifttt specs)
    - use more than one type of specs
  - for details, see `wip-test-smm-synth-rules` in `pkg/fsmsynth/test/lang.rkt`

- evsc spec synth
  ```graphviz
  digraph G {
  say_hello -> say_nice [ label = "(equal? sayDone hello)" ]
  ready -> say_hello [ label = "(and (equal? sayDone nice to meet you) (equal? faceVis #t))" ]
  say_nice -> say_bye [ label = "(and (equal? start #t) (equal? sayDone nice to meet you))" ]
  }
  ```

- hybrid (combined) spec synth
  ```graphviz
  digraph G {
  say_hello -> say_nice [ label = "(equal? faceVis #t)" ]
  say_nice -> say_bye [ label = "(equal? sayDone nice to meet you)" ]
  ready -> say_hello [ label = "(equal? faceVis #t)" ]
  }
  ```

- minor problems
  - `(faceVis . ,(from-diagram "ttt-t-")) ; not "t-----" because of spec-evsc1`
  - `(choose* #t #f) ; can't add (smempty) yet`

## New Directions

### Review: Common Skill Building Steps

A monologue-like skill:

1. Add monologue
2. Add speaking condition, e.g., speak only when a person is visible
3. Add arrival/departure handling
4. Add backchannel methods

_Q: are the code associated with each step independent?_

In general,

- contents data
- interaction structure (monologue, instruction)
  - with some variabilities, e.g., transition conditions

### New Problem

What is the research question?

### Idea1: Bodystorming-based Synthesis

- thought about why synthesis is helpful; it boiled down to when the system needs to consider many possible programs before the user provide any inputs
  - if a unique program can be generated given user input, the template approach (or the generative approach) is enough

- in the bodystorming scenario, the system cannot generate a program from demonstrations alone; some generalization is needed

- my approach would be
  - create a sketch (plus default constraints) that can map most implied programs demonstrated by users
  - devise a way of resolving ambiguity (underspecification) and conflicts

### Idea2: Declarative Behavior Generation Language

Motivation - NRI grant paper

Relate work

- https://idyll-lang.org/gallery
- https://www.react-spring.io/docs/hooks/basics
- https://github.com/superscriptjs/superscript/wiki
- https://www.emotionalabcs.com/?utm_source=facebook&utm_medium=social&utm_campaign=interests_assoc_teacher&utm_content=zen_green_moody_static
- activity

Limitations

- data crawling and mapping to structured data
- underlying representation / language, accessibility


---


# Week 8

- During the last meeting, we re-visited our reasons for trying out a synthesis technique. Specifically, we asked ourselves the "what problem do we want to solve?" question.

## Proposal

- The problem: creating HRI programs is hard because a programmer needs to
  - understand and apply common interaction conventions, which may vary across contexts
  - follow specs required by the multiple stakeholders

- Story #1
  - a pre-school teacher wants to create a storytelling program
  - the teacher just wants to type in the story without "programming"; obviously, they don't want to worry about programming edge cases, etc.
    - the HRI program synthesis can be used to turn "script" into a "program"
    - _the system creates the program depends on the context, e.g., ways for handling interruption, arrival/departure_ e.g., based on specifications provided the robot system designer
  - after using the program for a week, the teacher wants to make the robot more engaging by making it respond to children's comments.
    - _the system adds it_
  - The teacher also wants to change the way the robot handles arrival or departure (e.g., don't say anything).
    - _the system identifies the conflicts_

- Story #1.5
  - the robot's capability changed, e.g., no voice, just screen
    - use texts to automatically figure out how timeout duration to add

- Story #2
  - a parent want to make "Simon says" game
    - simon says look-left, look-right, etc.
  - key idea - demos "parallelism"

- Story #2.5
  - creating a program from a DBT workbook
    - the system figures out how to distribute sentences

## Challenges

- What are the core contributions?
  - Why not template?

- MISC.
  - How is it going to help my job search?
