# ps4hri

For background, see the WIP paper https://www.overleaf.com/project/5da33f493aaf8000014aaf98.

- [docs](./docs): a collection of notes
- [hrisynth](./hrisynth): frpsynth-based hri program synthesizer
- [pkgs](./pkgs): a collection of custom racket packages saved as submodules
- [paper](./paper): the paper overleaf submodule
- [sandbox](./sandbox): a sandbox for running hri programs
- [speclang](./speclang): spec DSLs
- [webapp](./webapp): a synthesizer interface
