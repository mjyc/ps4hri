const xs = require("xstream").default;
const { runWithXStreams } = require("../src/makeControllerComponent");

test("runWithXStreams", () => {
  const output = runWithXStreams("xs");
  const expected = xs;
  Object.keys(output).map(k => expect(output[k]).toEqual(expected[k]));
});
