const spawn = require("child_process").spawn;
const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

// https://jonathanmh.com/how-to-enable-cors-in-express-js-node-js/
// https://stackoverflow.com/questions/18310394/no-access-control-allow-origin-node-apache-port-issue
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.post("/synthesizer", (req, res) => {
  // const p = spawn(
  //   "racket",
  //   ["../demo/tools/synthesizer.rkt", JSON.stringify(req.body)],
  //   {
  //     stdio: ["ignore", "pipe", "ignore"]
  //   }
  // );
  // p.stdout.on("data", data => {
  //   res.send(JSON.parse(data));
  // });
  const query = {
    bound: 4,
    "input-count": 1,
    "trace-list": [{ inputs: ["-13-7"], output: "-24-8" }],
    "insn-count": 1
  };
  const p = spawn(
    "racket",
    // ["../demo/tools/synthesizer.rkt", JSON.stringify(req.body)],
    ["../frpsynthesis/rxjsdsl/discretetime/main.rkt", JSON.stringify(query)],
    {
      stdio: ["ignore", "pipe", "ignore"]
    }
  );
  p.stdout.on("data", data => {
    res.send(JSON.parse(data));
  });
});

app.listen(port, () => console.log(`server is running on port ${port}`));
