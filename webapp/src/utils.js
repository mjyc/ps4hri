const xs = require("xstream").default;
const pairwise = require("xstream/extra/pairwise").default;
xs.prototype.pairwise = function() {
  return this.compose(pairwise);
};
const dropRepeats = require("xstream/extra/dropRepeats").default;
xs.prototype.distinctUntilChanged = function(isEqual) {
  return this.compose(dropRepeats(isEqual));
};

function runWithXStreams(code) {
  return Function(`"use strict";return (function(xs) {return ${code}})`)()(xs);
}

function withCancel(App, cancel$) {
  const wrappedApp = sources => {
    const wrappedSources = Object.keys(sources).reduce((prev, k) => {
      prev[k] = xs
        .merge(xs.of(sources[k]), cancel$.mapTo(xs.never()))
        .flatten();
      return prev;
    }, {});
    const sinks = App(wrappedSources);
    const wrappedSinks = Object.keys(sinks).reduce((prev, k) => {
      prev[k] = xs.merge(xs.of(sinks[k]), cancel$.mapTo(xs.never())).flatten();
      return prev;
    }, {});
    return wrappedSinks;
  };
  return wrappedApp;
}

function makeController(sourceNames, sinkNames) {
  return function(sources, command$) {
    const cancel$ = xs.create();
    const streamsOfSinks = sinkNames.reduce((prev, name) => {
      prev[name] = xs.create();
      return prev;
    }, {});

    command$.subscribe({
      next: cmd => {
        if (cmd.type === "load") {
          cancel$.shamefullySendNext({});
          const App = withCancel(runWithXStreams(cmd.value), cancel$);
          const appSinks = App(sources);
          sinkNames.map(k => {
            streamsOfSinks[k].shamefullySendNext(appSinks[k]);
          });
        } else if (cmd.type === "cancel") {
          cancel$.shamefullySendNext({});
        } else {
          console.warn("Invalid type", cmd.type);
        }
      }
    });

    return Object.keys(streamsOfSinks).reduce((prev, k) => {
      prev[k] = streamsOfSinks[k].flatten();
      return prev;
    }, {});
  };
}

module.exports = {
  runWithXStreams: runWithXStreams,
  withCancel: withCancel,
  makeController: makeController
};
