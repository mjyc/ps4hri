import "./styles.css";

import xs from "xstream";
import sampleCombine from "xstream/extra/sampleCombine";
import { run } from "@cycle/run";
import { withState } from "@cycle/state";
import { div, button, label, span, textarea, makeDOMDriver } from "@cycle/dom";
import { makeHTTPDriver } from "@cycle/http";
import {
  initializeTabletFaceRobotDrivers,
  withTabletFaceRobotActions
} from "@cycle-robot-drivers/run";
import { makeController } from "./utils";

function intent(domSource, httpSource) {
  return xs.merge(
    domSource
      .select("textarea.spec-editor")
      .events("input")
      .map(ev => ({
        type: "update_input_specs",
        value: {
          type: ev.target.dataset.type,
          index: ev.target.dataset.index,
          value: ev.target.value
        }
      })),
    httpSource
      .select("synthesizer")
      .flatten()
      .map(res => ({
        type: "update_outputs",
        value: res.body
      }))
  );
}

function model(action$) {
  return action$.fold(
    (prev, input) => {
      if (input.type === "update_input_specs") {
        const inputTraces = input.value.value
          .split("\n")
          .reduce((prev, text, i) => {
            const [key, val] = text.split(/[ ]+|:[ ]+/);
            if (typeof key === "undefined" || typeof val === "undefined") {
              return prev;
            }
            prev[key] = val;
            return prev;
          }, {});
        prev.inputSpecs[input.value.index][
          `${input.value.type}Traces`
        ] = inputTraces;
        return {
          inputSpecs: prev.inputSpecs,
          outputProgram: prev.outputProgram,
          candidatePrograms: prev.candidatePrograms
        };
      } else if (input.type === "update_outputs") {
        return {
          inputSpecs: prev.inputSpecs,
          outputProgram: input.value.program1,
          candidatePrograms: prev.candidatePrograms
        };
      } else {
        return prev;
      }
    },
    {
      inputSpecs: [
        {
          inputTracesStr: "",
          inputTraces: {},
          outputTracesStr: "",
          outputTraces: {}
        },
        {
          inputTracesStr: "",
          inputTraces: {},
          outputTracesStr: "",
          outputTraces: {}
        },
        {
          inputTracesStr: "",
          inputTraces: {},
          outputTracesStr: "",
          outputTraces: {}
        },
        {
          inputTracesStr: "",
          inputTraces: {},
          outputTracesStr: "",
          outputTraces: {}
        }
      ],
      outputProgram: "pane3: output_program",
      candidatePrograms: "pane4: candidate_programs"
    }
  );
}

function view(state$) {
  return state$.map(state =>
    div(".body", [
      div(".navbar", [
        span(".btn-group", [
          label("go to "),
          button(".robot", "robot face"),
          button(".camera", "camera feed")
        ]),
        span(".btn-group", [
          label("do "),
          button(".synth", "synthesize"),
          button(".run", "run"),
          button(".stop", "stop")
        ])
      ]),
      div(".main", [
        div(".col", [
          ...state.inputSpecs.map((inputSpec, i) =>
            div(".pane", [
              div(".col", [
                label(`input/output traces spec #${i + 1}`),
                ...["input", "output"].map(name =>
                  div(".pane", [
                    textarea(
                      ".spec-editor",
                      {
                        attrs: {
                          placeholder: `${name} traces`,
                          "data-index": i,
                          "data-type": `${name}`
                        }
                      },
                      inputSpec[`${name}TracesStr`]
                    )
                  ])
                )
              ])
            ])
          )
        ]),
        div(".col", [
          div(".pane", [
            textarea({ attrs: { disabled: true } }, state.outputProgram)
          ]),
          div(".pane", [
            textarea({ attrs: { disabled: true } }, state.candidatePrograms)
          ])
        ])
      ])
    ])
  );
}

function main(sources) {
  const action$ = intent(sources.DOM, sources.HTTP);
  const state$ = model(action$);
  const vdom$ = view(state$);

  const request$ = sources.DOM.select(".synth")
    .events("click")
    .compose(sampleCombine(state$))
    .map(([_, state]) => {
      const query = {
        traces: state.inputSpecs.map((inputSpec, i) => ({
          inputs: inputSpec.inputTraces,
          outputs: inputSpec.outputTraces
        }))
      };
      return {
        url: "http://127.0.0.1:3000/synthesizer",
        category: "synthesizer",
        method: "POST",
        send: query,
        headers: {
          "Content-Type": "application/json"
        }
      };
    });

  const scroll$ = xs.merge(
    sources.DOM.select(".robot")
      .events("click")
      .mapTo(sources.DOM.select(".face").element())
      .flatten(),
    sources.DOM.select(".camera")
      .events("click")
      .mapTo(sources.DOM.select(".posenet").element())
      .flatten()
  );

  sources.PoseDetection.events("poses").addListener({ next: x => null });
  const command$ = xs.merge(
    sources.DOM.select(".run")
      .events("click")
      .compose(sampleCombine(state$))
      .map(([_, state]) => ({
        type: "load",
        value: state.outputProgram
      })),
    sources.DOM.select(".stop")
      .events("click")
      .mapTo({
        type: "cancel"
      })
  );
  const controllerSinks = makeController(["poses"], ["say"])(
    {
      poses: sources.PoseDetection.events("poses")
    },
    command$
  );
  const robotSinks = {
    RobotSpeechbubbleAction: { goal: controllerSinks.say.map(x => String(x)) }
  };

  return {
    DOMtmp: vdom$,
    HTTP: request$,
    ScrollIntoView: scroll$,
    ...robotSinks
  };
}

function scrollIntoViewDriver(elem$) {
  elem$.addListener({
    next: elem => elem.scrollIntoView()
  });
}

const drivers = {
  ...initializeTabletFaceRobotDrivers(),
  HTTP: makeHTTPDriver(),
  ScrollIntoView: scrollIntoViewDriver
};

run(sources => {
  const sinks = withState(withTabletFaceRobotActions(main))(sources);
  return {
    ...sinks,
    DOM: xs.combine(sinks.DOMtmp, sinks.DOM).map(vdoms => div(vdoms))
  };
}, drivers);
