const execSync = require("child_process").execSync;
const fs = require("fs");

const specs = process.argv[2];
const cmd = `racket ./tools/synthesizer.rkt ${
  typeof specs !== "undefined" ? specs : ""
}`;
let code = "";
try {
  code = execSync(cmd).toString();
} catch (e) {
  console.error(`Error from ${cmd}`);
  process.exit(1);
}

fs.writeFileSync("./src/App.js", code);
